zonejm.dev CV site
==================
[![Netlify Status](https://api.netlify.com/api/v1/badges/8b46b97b-2c80-42ae-8495-f8a6ca1056c7/deploy-status)](https://app.netlify.com/sites/hardcore-volhard-ef7461/deploys)

Built with Next.js, Tailwind CSS.

Hosted on Netlify.
