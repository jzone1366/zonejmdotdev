import * as React from 'react'
import Image from 'next/image'

type Props = {
  title: string;
  isCurrent?: boolean;
  desc: string;
  labels: Array<string>;
  imagePath: string;
  altText: string;
}

const Card = (props: Props) => {
  const {
    title,
    isCurrent,
    desc,
    labels,
    imagePath,
    altText
  } = props;

  return (
    <div className="bg-white max-w-sm rounded overflow-hidden shadow-lg mx-2 mb-4 w-full md:w-1/2 lg:w-1/3 xl:w-1/3">
      <img
        className="w-full"
        src={imagePath}
        alt={altText}
      />
      <div className="px-6 py-4 relative">
        { isCurrent ?
          <span className="inline-block bg-orange-500 rounded-full px-2 py-1 text-xs font-semibold text-white absolute right-10 -top-10"> Current </span> :
          null
        }
        <h4 className="font-bold text-xl mb-2 text-center">
          { title }
        </h4>
        <p> { desc } </p>
      </div>
      <div className="px-6 py-4 text-center">
        {
        labels.map(label => {
          return (
            <span key={label} className="inline-block bg-blue-900 rounded-full px-3 py-1 my-2 text-sm font-semibold text-white mr-2">
              { label }
            </span>
          );
        })
      }
      </div>
    </div>
  );
}

export default Card;
