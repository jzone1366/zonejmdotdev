import * as React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHtml5, faCss3Alt, faJava, faJsSquare, faLess, faSass, faReact, faPython, faDocker, faPhp, faVuejs } from '@fortawesome/free-brands-svg-icons';

const SkillsIcons = () => (
    <div className="flex flex-wrap justify-center text-white max-w-xl m-auto">
        <div className="w-1/2 sm:w-1/3 md:w-1/3 lg:w-1/4 xl:w-1/4 flex justify-center">
            <FontAwesomeIcon icon={faJsSquare} size="6x"/>
        </div>
        <div className="w-1/2 sm:w-1/3 md:w-1/3 lg:w-1/4 xl:w-1/4 flex justify-center">
            <FontAwesomeIcon icon={faReact} size="6x"/>
        </div>
        <div className="w-1/2 sm:w-1/3 md:w-1/3 lg:w-1/4 xl:w-1/4 flex justify-center">
            <FontAwesomeIcon icon={faVuejs} size="6x" />
        </div>
        <div className="w-1/2 sm:w-1/3 md:w-1/3 lg:w-1/4 xl:w-1/4 flex justify-center">
            <FontAwesomeIcon icon={faHtml5} size="6x"/>
        </div>
        <div className="w-1/2 sm:w-1/3 md:w-1/3 lg:w-1/4 xl:w-1/4 flex justify-center">
            <FontAwesomeIcon icon={faCss3Alt} size="6x"/>
        </div>
        <div className="w-1/2 sm:w-1/3 md:w-1/3 lg:w-1/4 xl:w-1/4 flex justify-center">
            <FontAwesomeIcon icon={faLess} size="6x" />
        </div>
        <div className="w-1/2 sm:w-1/3 md:w-1/3 lg:w-1/4 xl:w-1/4 flex justify-center">
            <FontAwesomeIcon icon={faSass} size="6x" />
        </div>
        <div className="w-1/2 sm:w-1/3 md:w-1/3 lg:w-1/4 xl:w-1/4 flex justify-center">
            <FontAwesomeIcon icon={faJava} size="6x" />
        </div>
        <div className="w-1/2 sm:w-1/3 md:w-1/3 lg:w-1/4 xl:w-1/4 flex justify-center">
            <FontAwesomeIcon icon={faPython} size="6x" />
        </div>
        <div className="w-1/2 sm:w-1/3 md:w-1/3 lg:w-1/4 xl:w-1/4 flex justify-center">
            <FontAwesomeIcon icon={faPhp} size="6x" />
        </div>
        <div className="w-1/2 sm:w-1/3 md:w-1/3 lg:w-1/4 xl:w-1/4 flex justify-center">
            <FontAwesomeIcon icon={faDocker} size="6x" />
        </div>
    </div>
)

export default SkillsIcons
