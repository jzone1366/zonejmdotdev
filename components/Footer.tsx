import * as React from 'react'

const Footer = () => (
    <footer className="bg-gray-400 py-4 border-t-2 border-solid border-black">
        <div className="container mx-auto">
            <div className="text-center text-xl">
                Designed & Developed by zonejm
            </div>
            <p className="text-center text-xl text-blue-600 ">
                <a className="hover:text-blue-900" href="https://gitlab.com/jzone1366/zonejmdotdev" target="_blank" rel="noreferrer">Source</a> |
                <a className="hover:text-blue-900" href="https://www.netlify.com/" target="_blank" rel="noreferrer"> Hosted on Netlify </a> |
                <a className="hover:text-blue-900" href="https://nextjs.org/" target="_blank" rel="noreferrer"> Built with Next.js </a>
            </p>
        </div>
    </footer>
)

export default Footer
