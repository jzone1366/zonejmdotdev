import * as React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faLinkedin, faGithub, faGitlab } from '@fortawesome/free-brands-svg-icons';

const SocialIcons = () => (
    <div className="text-center text-white text-3xl mt-5">
        <a target="_blank" href="https://www.linkedin.com/in/zonejm/" rel="noreferrer">
            <FontAwesomeIcon icon={faLinkedin} className="mr-4 opacity-50 hover:opacity-100 transform hover:scale-150" />
        </a>
        <a target="_blank" href="https://www.github.com/jzone1366" rel="noreferrer">
            <FontAwesomeIcon icon={faGithub} className="mr-4 opacity-50 hover:opacity-100 transform hover:scale-150"/>
        </a>
        <a target="_blank" href="https://gitlab.com/jzone1366" rel="noreferrer">
            <FontAwesomeIcon icon={faGitlab} className="mr-4 opacity-50 hover:opacity-100 transform hover:scale-150"/>
        </a>
    </div>
)

export default SocialIcons;
