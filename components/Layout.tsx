import * as React from 'react';

type LayoutProps = {
  children: React.ReactNode
}

const Layout = ({ children }: LayoutProps) => {
    return (
        <div className="h-screen">
            <div className="mx-auto">
                { children }
            </div>
        </div>
    )
}

export default Layout;
