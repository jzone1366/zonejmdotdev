import * as React from 'react'
import type { NextPage } from 'next'
import Head from 'next/head'
import Layout from '../components/Layout'
import SocialIcons from '../components/SocialIcons'
import Card from '../components/Card'
import SkillsIcons from '../components/SkillsIcons'
import Footer from '../components/Footer'

const Home: NextPage = () => {
  return (
    <div>
      <Head>
        <title> JMZ | Software Engineer, Lead</title>
        <meta name="description" content="Atlanta, Georgia base Lead Software Engineer." />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Layout>
        <section className="py-16 bg-blue-900 border-b-2 border-black">
          <div className="container mx-auto">
            <h1 className="text-5xl text-center text-white"> Joshua M. Zone </h1>
            <h3 className="text-3xl text-center text-white mt-1"> Software Engineer, Lead </h3>
            <h3 className="text-3xl text-center text-white mt-1"> Metro Atlanta, GA </h3>
            <p className="text-xl text-center text-white mt-2"> Father, Husband, Data Nerd, Software Engineer, Physics Lover, Photographer </p>
          </div>
          <SocialIcons />
        </section>

        <section className="py-16 bg-gray-400">
          <div className="flex flex-wrap items-stretch justify-around sm:justify-center container mx-auto">
             <Card
              title="Nathan James"
              isCurrent
              desc={"Design and Build an ERP and WMS for an ecommerce furniture company."}
              labels={["JavaScript", "HTML", "CSS", "Vue", "PHP", "Laravel"]}
              imagePath="/images/nj.png"
              altText="Nathan James website homepage"
            />
             <Card
              title="PlayVS"
              desc={"Work with designers and engineers to develop an ESport platform for High Schoolers."}
              labels={["TypeScript", "JavaScript", "HTML", "CSS", "React"]}
              imagePath="/images/PlayVS.png"
              altText="PlayVS.com Website Homepage"
            />
             <Card
              title="PriceSpider"
              desc={"Work on a Where To Buy Widget that can be embedded in a Webpage. Improve and enhance UI/UX. Work on POC for future implementations."}
              labels={["JavaScript", "HTML", "CSS", "React", "Node", "C#"]}
              imagePath="/images/pricespider.png"
              altText="Pricespider.com website homepage"
            />
            <Card
              title="SoftVision"
              desc={"Worked with a client in the Healthcare Industry. Developed a Real Time Dashboard to show Health Statistics."}
              labels={["JavaScript", "HTML", "CSS", "React", "Angular", "Node"]}
              imagePath="/images/softvision.png"
              altText="Softvision website homepage"
            />
            <Card
              title="Reflex Media"
              desc={"Social Application redesign using React and PHP. Created new features and enhanced overall UX."}
              labels={["JavaScript", "HTML", "CSS", "React", "PHP"]}
              imagePath="/images/reflexmedia.png"
              altText="Reflex Media website homepage"
            />
            <Card
              title="Vegas.com"
              desc={"E-commerce site that focuses on travel to Las Vegas. Worked with tools such as SiteSpect to implement A/B tests, fix bugs and enhance overall UX"}
              labels={["JavaScript", "HTML", "CSS", "JQuery", "React"]}
              imagePath="/images/vdc.png"
              altText="Vegas.com website homepage"
            />
            <Card
              title="Virtusa | Polaris"
              desc={"Created Real Time tools for Traders in both the Private and Public sectors. Designed and Developed a POC for a complete application redesign."}
              labels={["JavaScript", "HTML", "CSS", "Java"]}
              imagePath="/images/virtusa.png"
              altText="Virtusa.com Website Homepage"
            />
          </div>
        </section>

        <section className="py-16 bg-blue-900 border-t-2 border-black">
          <h1 className="text-3xl text-center text-white pb-6"> Skills! </h1>
          <SkillsIcons />
        </section>
        <Footer />

      </Layout>
    </div>
  )
}

export default Home
